module V1
  # Definition of /v1/regions endpoints
  # @see https://github.com/cerebris/jsonapi-resources/tree/rails5#resources
  class RegionResource < BaseResource
    attributes :name, :code

    relationship :products, to: :many
  end
end
