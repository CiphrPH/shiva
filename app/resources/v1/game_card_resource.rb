module V1
  # Definition of /v1/game-cards endpoints
  # @see https://github.com/cerebris/jsonapi-resources/tree/rails5#resources
  class GameCardResource < ProductResource
    relationship :region, to: :one
  end
end
