module V1
  # Definition of /v1/products endpoints
  # @see https://github.com/cerebris/jsonapi-resources/tree/rails5#resources
  class ProductResource < BaseResource
    immutable

    attributes :name, :description, :price_centavos

    relationship :platform, to: :one
  end
end
