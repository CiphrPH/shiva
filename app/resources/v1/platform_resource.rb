module V1
  # Definition of /v1/platforms endpoints
  # @see https://github.com/cerebris/jsonapi-resources/tree/rails5#resources
  class PlatformResource < BaseResource
    attributes :name

    relationship :products, to: :many
  end
end
