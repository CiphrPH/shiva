module V1
  # The base resource for application-wide definitions.
  # All resources must inherit from this.
  # @see https://github.com/cerebris/jsonapi-resources/tree/rails5#resources
  class BaseResource < JSONAPI::Resource
    include JSONAPI::Authorization::PunditScopedResource
    abstract
  end
end
