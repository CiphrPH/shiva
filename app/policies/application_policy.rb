# This is the main policy for authorization. We're using
# pundit for this. All policies created in app/policies
# must inherit from ApplicationPolicy.
# @see https://github.com/elabs/pundit/tree/v1.1.0#policies
class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    false
  end

  def show?
    scope.where(id: record.id).exists?
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    false
  end

  def edit?
    update?
  end

  def destroy?
    false
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  # Scope policies are a way to lock ActiveRecord scopes inside
  # pundit policies. All scope policies created in app/policies
  # should inherit from Scope.
  # @see https://github.com/elabs/pundit/tree/v1.1.0#scopes
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
