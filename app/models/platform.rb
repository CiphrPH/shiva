# Platform of the products that are being sold
# @example
#   PC, Steam, Garena
class Platform < ApplicationRecord
  validates :name, presence: true
  has_many :products
end
