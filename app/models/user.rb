# This is the representation of all users
class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable

  attr_accessor :login
  enum role: [:user, :manager, :admin]

  validates :username,
            presence: true,
            length: { minimum: 6, maximum: 20 },
            uniqueness: { case_sensitive: false },
            format: {
              with: /^[a-zA-Z0-9_]*$/,
              multiline: true,
              message: 'Only allow letters, numbers, underscores'
            }

  # Override of Devise's #find_for_database_authentication
  # to enable logging in by both username and email
  #
  # @param warden_conditions [Hash] Authentication keys
  # @return [User] The user record
  # @see https://git.io/vVlHp
  # @example Get a user by email
  #   User.find_for_database_authentication(email: 'test@example.com')
  # @example Get a user by username
  #   User.find_for_database_authentication(username: 'testuser')
  # @example Get a user by email or username
  #   User.find_for_database_authentication(login: 'test@example.com')
  #   User.find_for_database_authentication(login: 'testuser')
  def self.find_for_database_authentication(warden_conditions)
    login = warden_conditions.delete(:login)

    if login
      where(warden_conditions.to_hash).where(
        ['lower(username) = :value OR lower(email) = :value',
         { value: login.downcase }]).first
    elsif warden_conditions.key?(:username) || warden_conditions.key?(:email)
      warden_conditions[:email].downcase! if warden_conditions[:email]
      where(warden_conditions.to_hash).first
    end
  end
end
