# Region for products
# Some of our products are locked to certain regions
# @example
#   [[Europe, EU], [North America, NA]]
class Region < ApplicationRecord
  validates :name, :code, presence: true

  has_many :game_cards
end
