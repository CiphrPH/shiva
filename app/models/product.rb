# The products that we sell in the site
# This is the base class for all of types of products
class Product < ApplicationRecord
  monetize :price_centavos

  belongs_to :platform

  validates :name, presence: true
end
