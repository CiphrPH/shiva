# Base model class
# All models should inherit from this class
# instead of ActiveRecord::Base from Rails 5 onwards
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
