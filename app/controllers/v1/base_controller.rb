module V1
  # All API related controllers must inherit from BaseController
  class BaseController < ApplicationController
    include JSONAPI::ActsAsResourceController

    before_action :doorkeeper_authorize!, only: [:create, :update, :destroy]

    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    private

    def context
      { user: current_resource_owner }
    end

    def current_resource_owner
      User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
    end

    def handle_exceptions(e)
      if JSONAPI.configuration.exception_class_whitelist
                .any? { |k| e.class.ancestors.include?(k) }
        raise e
      else
        super
      end
    end

    def user_not_authorized
      render json: { error: 'Unauthorized', code: 401 }, status: :unauthorized
    end
  end
end
