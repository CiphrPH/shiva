# Base Mailer class
# All Mailer classes should inherit from this class
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
