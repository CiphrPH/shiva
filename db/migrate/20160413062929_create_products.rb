class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name, default: ''
      t.string :type, default: 'Product'
      t.integer :platform_id
      t.monetize :price, default: 0
      t.text :description, default: ''

      t.timestamps
    end
  end
end
