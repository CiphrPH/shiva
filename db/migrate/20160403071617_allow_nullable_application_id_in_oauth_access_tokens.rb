class AllowNullableApplicationIdInOauthAccessTokens < ActiveRecord::Migration[5.0]
  def up
    change_column :oauth_access_tokens, :application_id, :integer, null: true
  end

  def down
    change_column :oauth_access_tokens, :application_id, :integer, null: false
  end
end
