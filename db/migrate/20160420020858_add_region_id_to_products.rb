class AddRegionIdToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :region_id, :integer
  end
end
