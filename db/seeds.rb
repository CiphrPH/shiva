# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

admin = User.new(email: 'admin@ciphr.ph',
                 password: 'password',
                 password_confirmation: 'password',
                 username: 'ciphradmin',
                 birth_date: Date.today)
admin.confirm!
admin.admin!
admin.save

PLATFORMS = ['Steam', 'Xbox Live', 'Playstation Network', 'Battle.net']
PLATFORMS.each do |platform|
  Platform.create(name: platform)
end

REGIONS = [['Southeast Asia', 'SEA'], ['North America', 'NA'], ['Europe', 'EU'], ['Asia', 'Asia']]
REGIONS.each do |name, code|
  Region.create(name: name, code: code)
end
