# Shiva

This is the backend of http://ciphr.ph

## Setting up

```
$ git clone git@gitlab.com:ciphr/shiva.git
$ cd shiva
$ bundle install
$ cp config/database.yml.sample config/database.yml
$ vi config/database.yml # Edit this file according to your PostgreSQL configuration
$ cp config/secrets.yml.sample config/secrets.yml
$ vi config/secrets.yml # Generate a new secret using `rails secret` and edit the keys
$ rails db:create db:migrate db:seed
```

**Tip**: The lazy way of creating a user in PostgreSQL is `$ createuser --createdb username`.
This will setup a user called `username` with no password and has the ability to create databases.

## Testing

We're using [Minitest](https://github.com/seattlerb/minitest) as the test framework
and [Rubocop](https://github.com/bbatsov/rubocop) for code analysis. Both of these
libraries run through [Guard](https://github.com/guard/guard):

```
$ bundle exec guard
```

## Deployment

### Initial Deployment

On initial deployment, make sure to do finish provisioning the server via [Hydaelyn](https://gitlab.com/ciphr/hydaelyn)
before doing anything else.

```
# Upload config/database.yml and config/secrets.yml and edit them accordingly on the server
$ bundle exec cap <production/staging> linked_files:upload_files

# Do the initial deployment
$ bundle exec cap <production/staging> deploy:initial

# Seed the database to get started
$ bundle exec cap <production/staging> deploy:seed
```

### Subsequent Deployments

Subsequent deployments can be done using the following command:

```
$ bundle exec cap <production/staging> deploy
```
