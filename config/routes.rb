Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users

  namespace :v1 do
    jsonapi_resources :products
    jsonapi_resources :game_cards
  end
end
