source 'https://rubygems.org'

gem 'rails', '5.0.0'
gem 'pg', '~> 0.18'
gem 'puma', '3.4.0'

gem 'devise', '4.0.0.rc2'
gem 'doorkeeper', '4.0.0.rc2'
gem 'rack-cors', '0.4.0', require: 'rack/cors'
gem 'jsonapi-resources', '0.8.0.beta2'
gem 'pundit', '1.1.0'
gem 'jsonapi-authorization', '0.8.0'

gem 'rubocop', '~> 0.39.0', require: false

gem 'money-rails', '1.6.0'

group :development, :test do
  gem 'byebug'
end

group :development do
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'letter_opener', '1.4.1'
  gem 'guard', '2.13.0', require: false
  gem 'guard-minitest', '2.4.4', require: false
  gem 'guard-rubocop', '1.2.0', require: false
  gem 'rb-fsevent', '0.9.7', require: false

  # Guard notification on OS X notification popups
  gem 'terminal-notifier-guard', '1.7.0', :require => false

  gem 'capistrano', '3.4.0', require: false
  gem 'capistrano-rvm', '0.1.1', require: false
  gem 'capistrano-rails', '1.1.6', require: false
  gem 'capistrano-bundler', '1.1.4', require: false
  gem 'capistrano3-puma', '1.2.1', require: false
  gem 'capistrano-linked-files', '1.2.0', require: false
  gem 'capistrano-rails-console', '1.0.2', require: false
end

group :test do
  gem 'launchy', '2.4.3'
  gem 'minitest-reporters', '1.1.8'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
