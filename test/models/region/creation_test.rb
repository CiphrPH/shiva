require 'test_helper'

class RegionCreationTest < ActiveSupport::TestCase
  test 'succeeds creating a region' do
    Region.create(name: 'Region 3',
                  code: 'region_3')

    assert_equal 3, Region.count
  end

  test 'fails creating a platform with no name' do
    region = Region.new(name: '',
                        code: 'region_3')

    refute region.valid?
  end

  test 'fails creating a platform with no code' do
    region = Region.new(name: 'Region 3',
                        code: '')

    refute region.valid?
  end
end
