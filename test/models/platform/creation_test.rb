require 'test_helper'

class PlatformCreationTest < ActiveSupport::TestCase
  test 'succeeds creating a platform' do
    Platform.create(name: 'Platform 3')

    assert_equal 3, Platform.count
  end

  test 'fails creating a platform with no name' do
    platform = Platform.new(name: '')

    refute platform.valid?
  end
end
