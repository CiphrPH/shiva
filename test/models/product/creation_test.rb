require 'test_helper'

class ProductCreationTest < ActiveSupport::TestCase
  test 'succeeds creating a product' do
    Product.create(name: 'Product 1',
                   price: 200,
                   platform: Platform.first,
                   description: 'Product Description')

    assert_equal Product.count, 3
  end

  test 'fails creating a product with no name' do
    product = Product.new(name: '',
                          price: 200,
                          platform: Platform.first,
                          description: 'Product Description')

    refute product.valid?
    assert_includes product.errors.full_messages, "Name can't be blank"
  end

  test 'fails creating a product with no platform' do
    product = Product.new(name: 'Product 1',
                          price: 200,
                          description: 'Product Description')

    refute product.valid?
    assert_includes product.errors.full_messages, 'Platform must exist'
  end
end
