require 'test_helper'

class UserAuthenticationTest < ActiveSupport::TestCase
  test 'succeeds logging in through a username' do
    user = User.find_for_database_authentication(username: 'admin_user')

    assert_equal user, User.find(1)
    assert user.valid_password?('password')
  end
end
