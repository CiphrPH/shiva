require 'test_helper'

class UserAuthorizationTest < ActiveSupport::TestCase
  test 'defaults to user role' do
    user = User.create(email: 'test@example.com',
                       password: 'password',
                       password_confirmation: 'password',
                       username: 'testuser',
                       birth_date: Date.new(1990, 4, 21)
                      )

    assert_equal user.role, 'user'
    assert user.user?
  end

  test 'updates to a manager role' do
    user = User.create(email: 'test@example.com',
                       password: 'password',
                       password_confirmation: 'password',
                       username: 'testuser',
                       birth_date: Date.new(1990, 4, 21)
                      )
    user.manager!

    assert_equal user.role, 'manager'
    assert user.manager?
  end

  test 'updates to an admin role' do
    user = User.create(email: 'test@example.com',
                       password: 'password',
                       password_confirmation: 'password',
                       username: 'testuser',
                       birth_date: Date.new(1990, 4, 21)
                      )
    user.admin!

    assert_equal user.role, 'admin'
    assert user.admin?
  end
end
