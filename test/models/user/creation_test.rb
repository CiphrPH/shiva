require 'test_helper'

class UserCreationTest < ActiveSupport::TestCase
  test 'succeeds creating a user' do
    User.create(email: 'test@example.com',
                password: 'password',
                password_confirmation: 'password',
                username: 'testuser',
                birth_date: Date.new(1990, 4, 21)
               )

    assert_equal 4, User.count
  end

  test 'fails creating a user with no username' do
    user = User.new(email: 'test@example.com',
                    password: 'password',
                    password_confirmation: 'password',
                    birth_date: Date.new(1990, 4, 21)
                   )

    refute user.valid?
  end

  test 'fails creating a user with a short username' do
    user = User.new(email: 'test@example.com',
                    password: 'password',
                    password_confirmation: 'password',
                    username: 'test',
                    birth_date: Date.new(1990, 4, 21)
                   )

    refute user.valid?
  end

  test 'fails creating a user with a very long username' do
    user = User.new(email: 'test@example.com',
                    password: 'password',
                    password_confirmation: 'password',
                    username: 'testhelloworldhihowareyouyeahsure',
                    birth_date: Date.new(1990, 4, 21)
                   )

    refute user.valid?
  end

  test 'fails creating a user with an existing username' do
    user = User.new(email: 'test@example.com',
                    password: 'password',
                    password_confirmation: 'password',
                    username: 'normal_user',
                    birth_date: Date.new(1990, 4, 21)
                   )

    refute user.valid?
  end

  test 'fails creating a user with a variation of an existing username' do
    user = User.new(email: 'test@example.com',
                    password: 'password',
                    password_confirmation: 'password',
                    username: 'NoRmAl_UsEr',
                    birth_date: Date.new(1990, 4, 21)
                   )

    refute user.valid?
  end

  test 'fails creating a user with an email address as the username' do
    user = User.new(email: 'test@example.com',
                    password: 'password',
                    password_confirmation: 'password',
                    username: 'user1@example.com',
                    birth_date: Date.new(1990, 4, 21)
                   )

    refute user.valid?
  end

  test 'fails creating a user with an invalid string as the username' do
    user = User.new(email: 'test@example.com',
                    password: 'password',
                    password_confirmation: 'password',
                    username: 'user!@#<>-',
                    birth_date: Date.new(1990, 4, 21)
                   )

    refute user.valid?
  end
end
