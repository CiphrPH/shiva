require 'test_helper'

class GameCardControllerDestroyTest < ActionController::TestCase
  def setup
    @controller = V1::GameCardsController.new
    @request.content_type = 'application/vnd.api+json'
  end

  test 'succeeds destroying a game card as an admin' do
    admin_user = User.admin.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: admin_user.id).first

    delete :destroy, params: { id: 1, access_token: access_token.token }

    assert_response :no_content
    assert_equal Product.count, 1
  end

  test 'fails destroying a game card as a manager' do
    manager_user = User.manager.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: manager_user.id).first

    delete :destroy, params: { id: 1, access_token: access_token.token }

    assert_response :unauthorized
    assert_equal Product.count, 2
  end

  test 'fails destroying a game card as a normal user' do
    normal_user = User.user.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: normal_user.id).first

    delete :destroy, params: { id: 1, access_token: access_token.token }

    assert_response :unauthorized
    assert_equal Product.count, 2
  end
end
