require 'test_helper'

class GameCardControllerUnauthorizedCreateTest < ActionController::TestCase
  def setup
    @controller = V1::GameCardsController.new
    @request.content_type = 'application/vnd.api+json'
    @platform = Platform.first
    @region = Region.first
  end

  test 'fails creating a game card as a normal user' do
    normal_user = User.user.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: normal_user.id).first

    params = {
      data: {
        type: 'game-cards',
        attributes: {
          name: 'Steam Wallet 50',
          'price-centavos' => 50_00,
          description: 'This is a new game card! Yay!'
        },
        relationships: {
          platform: {
            data: {
              type: 'platforms',
              id: @platform.id
            }
          },
          region: {
            data: {
              type: 'regions',
              id: @region.id
            }
          }
        }
      },
      access_token: access_token.token
    }

    post :create, params: params

    assert_response :unauthorized
  end
end
