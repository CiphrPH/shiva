require 'test_helper'

class GameCardControllerCreateTest < ActionController::TestCase
  def setup
    @controller = V1::GameCardsController.new
    @request.content_type = 'application/vnd.api+json'
    @platform = Platform.first
    @region = Region.first
  end

  test 'succeeds creating a game card as an admin' do
    admin_user = User.admin.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: admin_user.id).first

    params = {
      data: {
        type: 'game-cards',
        attributes: {
          name: 'Steam Wallet 50',
          'price-centavos' => 50_00,
          description: 'This is a new game card! Yay!'
        },
        relationships: {
          platform: {
            data: {
              type: 'platforms',
              id: @platform.id
            }
          },
          region: {
            data: {
              type: 'regions',
              id: @region.id
            }
          }
        }
      },
      access_token: access_token.token
    }

    post :create, params: params
    json_response = JSON.parse(response.body)
    assert_response :created
    assert_equal json_response['data']['type'], 'game-cards'
    assert_equal json_response['data']['attributes']['name'], 'Steam Wallet 50'
    assert_equal json_response['data']['attributes']['price-centavos'], 50_00
  end

  test 'succeeds creating a game card as a manager' do
    manager_user = User.manager.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: manager_user.id).first

    params = {
      data: {
        type: 'game-cards',
        attributes: {
          name: 'Steam Wallet 50',
          'price-centavos' => 50_00,
          description: 'This is a new game card! Yay!'
        },
        relationships: {
          platform: {
            data: {
              type: 'platforms',
              id: @platform.id
            }
          },
          region: {
            data: {
              type: 'regions',
              id: @region.id
            }
          }
        }
      },
      access_token: access_token.token
    }

    post :create, params: params
    json_response = JSON.parse(response.body)

    assert_response :created
    assert_equal json_response['data']['type'], 'game-cards'
    assert_equal json_response['data']['attributes']['name'], 'Steam Wallet 50'
    assert_equal json_response['data']['attributes']['price-centavos'], 50_00
  end
end
