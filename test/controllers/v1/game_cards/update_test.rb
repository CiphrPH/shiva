require 'test_helper'

class GameCardControllerUpdateTest < ActionController::TestCase
  def setup
    @controller = V1::GameCardsController.new
    @request.content_type = 'application/vnd.api+json'
  end

  test 'succeeds updating a game card as an admin' do
    admin_user = User.admin.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: admin_user.id).first

    params = { id: 1,
               data: {
                 id: 1,
                 type: 'game-cards',
                 attributes: {
                   name: 'Updated Product'
                 }
               },
               access_token: access_token.token
             }

    patch :update, params: params
    json_response = JSON.parse(response.body)

    assert_response :ok
    assert_equal json_response['data']['attributes']['name'], 'Updated Product'
  end

  test 'succeeds updating a game card as a manager' do
    manager_user = User.manager.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: manager_user.id).first

    params = { id: 1,
               data: {
                 id: 1,
                 type: 'game-cards',
                 attributes: {
                   name: 'Updated Product'
                 }
               },
               access_token: access_token.token
             }

    patch :update, params: params
    json_response = JSON.parse(response.body)

    assert_response :ok
    assert_equal json_response['data']['attributes']['name'], 'Updated Product'
  end

  test 'fails updating a game card as a regular user' do
    regular_user = User.user.first
    access_token = Doorkeeper::AccessToken
                   .where(resource_owner_id: regular_user.id).first

    params = { id: 1,
               data: {
                 id: 1,
                 type: 'game-cards',
                 attributes: {
                   name: 'Updated Product'
                 }
               },
               access_token: access_token.token
             }

    patch :update, params: params

    assert_response :unauthorized
  end
end
