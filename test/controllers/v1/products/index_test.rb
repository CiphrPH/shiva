require 'test_helper'

class ProductsControllerIndexTest < ActionController::TestCase
  def setup
    @controller = V1::ProductsController.new
  end

  test 'succeeds loading the endpoint' do
    get :index
    assert_response :ok
  end

  test 'succeeds getting a list of products' do
    get :index
    assert_includes response.body, 'Steam Wallet 250'
    assert_includes response.body, 'Final Fantasy XIV 60 day Time Card'
    refute_includes response.body, 'PC'
  end

  test 'succeeds getting a list of products with the platform' do
    get :index, params: { include: 'platform' }
    assert_includes response.body, 'Steam'
    assert_includes response.body, 'PC'
  end
end
