require 'test_helper'

class ProductsControllerShowTest < ActionController::TestCase
  def setup
    @controller = V1::ProductsController.new
  end

  test 'succeeds loading the endpoint' do
    get :show, params: { id: 1 }
    assert_response :ok
  end

  test 'succeeds getting the product information' do
    get :show, params: { id: 1 }

    json_response = JSON.parse(response.body)
    assert_equal json_response['data']['id'], '1'
    assert_equal json_response['data']['attributes']['name'], 'Steam Wallet 250'
    assert_equal json_response['data']['attributes']['price-centavos'], 250_00
  end

  test 'succeeds getting the product and platform information' do
    get :show, params: { id: 1, include: 'platform' }

    json_response = JSON.parse(response.body)
    assert_equal json_response['included'][0]['type'], 'platforms'
    assert_equal json_response['included'][0]['attributes']['name'], 'Steam'
  end

  test 'fails getting a non-existent product' do
    get :show, params: { id: 44, include: 'platform' }
    assert_response :not_found
  end
end
