v0.4.1 (2016/08/21)

- Upgrade Ruby to 2.3.1 from 2.3.0
- Upgrade Rails to 5.0.0 from 5.0.0.beta3
- Refactor test assertions
- Upgrade jsonapi-resources to 0.8.0.beta2 from 0.7.0 rails5 branch
- Upgrade jsonapi-authorization to 0.8.0 from 0.6.0
- Add Capistrano plugin to upload secrets.yml and database.yml
- Use puma as the web server for development
- Add Capistrano plugin to perform `rails console` and `rake db:seed`
- Update schema to Rails 5

v0.4.0 (2016/04/26)

- Add Platform resource
- Add Product resource
- Add GameCard resource (STI from Product)
- Add Platform resource
- Add Region resource
- Use money-rails for handling currency
- Place all create/update/destroy endpoints behind Doorkeeper
- Add authorization rules for all API endpoints

v0.3.1 (2016/04/10)

- Add deployment configurations

v0.3.0 (2016/04/07)

- Use JSONAPI::Resources for building the API endpoints
- Add User roles through Rails enum (User, Manager, Admin)
- Refactor User tests into small, multiple test files to avoid rubocop
offenses
- Use pundit for User authorization

v0.2.1 (2016/04/03)

- Support CORS to allow frontend to talk to doorkeeper
- Only allow Resource Owner Password Credentials flow
- Allow oauth_access_tokens#application_id to be nullable, so we can authenticate in the frontend

v0.2.0 (2016/04/02)

- Use Doorkeeper for OAuth2 authentication
- Support Resource Owner Password Credentials flow to authenticate via login
and password

v0.1.1 (2016/04/02)

- Use guard to speed up local testing
- Use minitest-reporters to improve the output of tests
- Use guard-rubocop to execute rubocop when guard is running
- Fix rubocop offenses by refactoring User#find_for_database_authentication

v0.1.0 (2016/04/02)

- Use devise for user authentication
- Use letter_opener for opening emails in development
- Allow users to login via username alongside email
